#include "ros/ros.h"
#include "std_msgs/String.h"
#include "project_main/toggleService.h"

bool add(service::service1::Request  &req, service::service1::Response &res)
{
  res.sum = req.a + req.b;
  ROS_INFO("request: x=%ld, y=%ld", (long int)req.a, (long int)req.b);
  ROS_INFO("sending back response: [%ld]", (long int)res.sum);
  return true;
}

int main(int argc,char **argv)
{
   ros::init(argc, argv, "server_node");
   ros::NodeHandle nh;
   ros::Rate loop_rate(10);
   ros::ServiceServer service = nh.advertiseService("service1", add);
   ROS_INFO("Ready to add two ints.");

  while (ros::ok())
  {
  ros::spinOnce();
  loop_rate.sleep();
  }
  return 0;
}