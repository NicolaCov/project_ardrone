#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <project_main/dynparamsConfig.h>
#include "ar_pose/ARMarkers.h"
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>
#include "project_main/ToggleService.h"
#include "tf/transform_listener.h"

// global variables declaration
double x_vel, y_vel, z_vel, pitch, roll, yaw;
bool takeOff, landing, toggle;
double x, y, z, xd, yd, zd;
double ex,ey,ez,e_tot; // declaring errors


void callback(project_main::dynparamsConfig &config, uint32_t level) 
{
	x_vel = config.x_vel; 
	y_vel = config.y_vel; 
	z_vel = config.z_vel; 
	pitch = config.pitch; 
	roll = config.roll; 
	yaw = config.yaw; 

	takeOff = config.takeOff;
	landing = config.landing;
	
	//ROS_INFO("%f %f %f %f %f %f %d %d \n",x_vel,y_vel,z_vel,pitch,roll,yaw,takeOff,landing);
}


void error_pose()
{	
	ex=x-xd;
	ey=y-yd;
	ez=z-zd;
	e_tot=sqrt(pow(ex,2)+pow(ey,2)+pow(ez,2));  //2-norm of the error

	//ROS_INFO("Error: %f \n",e_tot);
  	//ROS_INFO("Error along x axis: %f \n Error along y axis: %f \n Error along z axis: %f",ex,ey,ez);
  	//ROS_INFO("[Desired position] \n x: %f \n y: %f \n z: %f \n",xd,yd,zd);
}


void Camera_Callback(const ar_pose::ARMarkers::ConstPtr& cam )
{
  //checking if the vector is not empty, if it is we cannot work on it
  if (!cam->markers.empty())
  {
  	x=cam->markers.at(0).pose.pose.position.x;
  	y=cam->markers.at(0).pose.pose.position.y;
  	z=cam->markers.at(0).pose.pose.position.z;
  
  	x_or = cam->markers.at(0).pose.pose.orientation.x;
  	y_or = cam->markers.at(0).pose.pose.orientation.y;
  	z_or = cam->markers.at(0).pose.pose.orientation.z;

  	//Converting quaternion to rpy
  	bool is_marker_ok = true;

  	tf::Quaternion qt;
  	double roll , pitch , yaw;
  	tf::quaternionMsgToTf(cam->pose.pose.orientation, qt);
  	tf::Matrix3x3(qt).getRPY(roll,pitch,yaw);

  	if(std::isnan(roll) || std::isfinite(roll) || (cam->pose.pose.position.z < 0) )
  		is_marker_ok = false;

  	ROS_INFO("roll: %d pitch: %d yaw: %d \n",roll,pitch,yaw);

    //I left this just to remember that we could extract also the pose ( expressed with quaternions)
	//ROS_INFO("or_x= [%f]", cam->markers.at(0).pose.pose.orientation.x);
	//ROS_INFO("or_y= [%f]", cam->markers.at(0).pose.pose.orientation.y);
	//ROS_INFO("or_z= [%f]", cam->markers.at(0).pose.pose.orientation.z);
	//ROS_INFO("or_w= [%f]", cam->markers.at(0).pose.pose.orientation.w);
	
 	error_pose(); //calling the function that computes the error
  }
  else
  {
  	ROS_INFO("\n\n\n No marker is detected");
  	ex = 0;
  	ey = 0;
  	ez = 0;
  	e_tot = 0;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "project_main_node");
	ros::NodeHandle n;
	ros::Rate loop_rate(10);

	dynamic_reconfigure::Server<project_main::dynparamsConfig> server;
	dynamic_reconfigure::Server<project_main::dynparamsConfig>::CallbackType f;
	f = boost::bind(&callback, _1, _2); //when the rqt reconfiguration has modified something the function "callback" is called
	server.setCallback(f);
	
	// general subscription to the ar_pose
	//ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, AR_pose_marker_Callback); //subscription to the topic "ar_pose_marker"
	
	ros::Publisher takeOff_pub = n.advertise<std_msgs::Empty>("/ardrone/takeoff",1);
	ros::Publisher land_pub = n.advertise<std_msgs::Empty>("/ardrone/land",1);
	ros::Publisher cmd_vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel",1);

	//we first subscribe to the bottomcamera, and then to the front one
	ros::Subscriber sub = n.subscribe("/ar_pose_bottom/ar_pose_marker", 1, Camera_Callback); //subscription to the topic "ar_pose_marker"

	ex=0;
	ey=0;
	ez=0;
	e_tot = 0;

    toggle = false;
	xd = 0;
	yd = 0;
	zd = 1.0;
	bool subscriptionDone = false;

	while (ros::ok())
	{
		
		if(toggle && !subscriptionDone) //change the topic to subscribe if toggle is true (if we are in the correct position)
		{
			sub = n.subscribe("/ar_pose_front/ar_pose_marker", 1, Camera_Callback); //subscription to the topic "ar_pose_marker"
			subscriptionDone = true;
		}
		
		//setting up the Empty msgs for the dynamic reconfiguration of the Landing and Takeoff
		std_msgs::Empty emptyMsg;

		if(takeOff && !landing)
		{
			takeOff_pub.publish(emptyMsg);
		}
		if(landing && !takeOff)
		{
			land_pub.publish(emptyMsg);
		}
		//setting up the Twist message
		geometry_msgs::Twist cmd_vel_msg;

		cmd_vel_msg.linear.x = x_vel;
		cmd_vel_msg.linear.y = y_vel;
		cmd_vel_msg.linear.z = z_vel;
		cmd_vel_msg.angular.x = pitch;
		cmd_vel_msg.angular.y = roll;
		cmd_vel_msg.angular.z = yaw;

		// simple bad proportional controller
		cmd_vel_msg.linear.x = -(float)ex*0.1;
		cmd_vel_msg.linear.y = -(float)ey*0.1;
		cmd_vel_msg.linear.z = -(float)ez*0.5;

		ROS_INFO("\n\n\nvel_x= [%f] \n\n\nvel_y= [%f] \n\n\nvel_z= [%f]", cmd_vel_msg.linear.x,cmd_vel_msg.linear.y,cmd_vel_msg.linear.z);
		
		cmd_vel_pub.publish(cmd_vel_msg);

		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
