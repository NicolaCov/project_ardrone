#include "ros/ros.h"
#include "std_msgs/String.h"
#include "project_main/ToggleService.h"

bool toogle;

bool callback(project_main::ToggleService::Request  &req, project_main::ToggleService::Request &res)
{
  ROS_INFO("Camera Toggled");
  toogle = true;
  return true;
}

int main(int argc,char **argv)
{
   ros::init(argc, argv, "ToggleService_node");
   ros::NodeHandle nh;
   ros::Rate loop_rate(10);
   toogle = false;
   ros::ServiceServer service = nh.advertiseService("ToggleService", callback);
   ROS_INFO("Ready to toggle the camera");

   //ros::param::set("/relative_param", "my_string");


  while (ros::ok())
  {
  ros::spinOnce();
  loop_rate.sleep();
  }
  return 0;
}