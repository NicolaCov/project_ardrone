#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <project_main/dynparamsConfig.h>
#include "ar_pose/ARMarkers.h"
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>
#include "std_srvs/Empty.h"
#include "tf/transform_listener.h"

// global variables declaration
double x_vel, y_vel, z_vel, pitch, roll, yaw;
bool takeOff, landing, toggle, activateController;
double x, y, z, xd, yd, zd , yaw_d , pitch_d;
double ex,ey,ez,e_yaw; // declaring errors
double ex_old, ey_old, ez_old, e_yaw_old , e_pitch,e_pitch_old;
double e_yaw_int =0;
double kp_x,kp_y,kp_z,kp_yaw;
double kd_x,kd_y,kd_z,kd_yaw;
double ki_x,ki_y,ki_z,ki_yaw;
double pid_x, pid_y, pid_z, pid_yaw;
bool randomVel;
bool controller=false;
bool inAir = false; // to say if the drone is flying (true) or not (false)
double sec_ini;
double sec_end;
double th = 0.5; //threshold for errors
double th_yaw = 0.5; //threshold for errors
double th_pitch = 0.5;
bool frontController = false; // false: bottom controller true: front controller activated

double lin_limit = 0.5;
double ang_limit = 0.5;

bool markerDetected=false;
int counterToggle = 0; //conter for the initial toggle  1:bottom  0:front

void callback(project_main::dynparamsConfig &config, uint32_t level) 
{
	x_vel = config.x_vel; 
	y_vel = config.y_vel; 
	z_vel = config.z_vel; 
	pitch = config.pitch; 
	roll = config.roll; 
	yaw = config.yaw; 

	if(!takeOff && config.takeOff)
		inAir = true;

	takeOff = config.takeOff;
	config.takeOff = false;


	landing = config.landing;
	config.landing = false;
	toggle = config.toggle;
	config.toggle = false;

	kp_x = config.kp_x;
	kp_y = config.kp_y;
	kp_z = config.kp_z;
	kp_yaw = config.kp_yaw;

	kd_x = config.kd_x;
	kd_y = config.kd_y;
	kd_z = config.kd_z;
	kd_yaw = config.kd_yaw;

	ki_x = config.ki_x;
	ki_y = config.ki_y;
	ki_z = config.ki_z;
	ki_yaw = config.ki_yaw;

	randomVel = config.randomVel;

	controller = config.controller;
	if(controller)
		config.activateController = false;

	activateController = config.activateController;
	if(activateController) // to avoid that the controller and activateController are enabled at the same time 
		config.controller = false;

	yaw_d = config.yaw_desired;
	//ROS_INFO("%f %f %f %f %f %f %d %d \n",x_vel,y_vel,z_vel,pitch,roll,yaw,takeOff,landing);
}

bool errorOk()
{
	//return sqrt(pow(ex,2)+pow(ey,2)+pow(ez,2)+pow(e_yaw,2)) < th && markerDetected;
	//return (fabs(ex)<th) && (fabs(ey)<th) && (fabs(ez)<th) && (fabs(e_yaw)<th_yaw) && markerDetected; 
	return (fabs(ex)<th) && (fabs(ey)<th) && (fabs(ez)<th) && (fabs(e_yaw)<th_yaw) && markerDetected; 

}
bool errorOk2()
{
	//return sqrt(pow(ex,2)+pow(ey,2)+pow(ez,2)+pow(e_yaw,2)) < th && markerDetected;
	//return (fabs(ex)<th) && (fabs(ey)<th) && (fabs(ez)<th) && (fabs(e_yaw)<th_yaw) && markerDetected; 
	return (fabs(ex)<th) && (fabs(ey)<th) && (fabs(ez)<th) && (fabs(e_pitch)<th_pitch) && markerDetected; 

}

void Camera_Callback(const ar_pose::ARMarkers::ConstPtr& cam )
{
  //checking if the vector is not empty, if it is we cannot work on it
    if (!cam->markers.empty())
    {
	  	x=cam->markers.at(0).pose.pose.position.x;
	  	y=cam->markers.at(0).pose.pose.position.y;
	  	z=cam->markers.at(0).pose.pose.position.z;
	   
	    //I left this just to remember that we could extract also the pose ( expressed with quaternions)
		//ROS_INFO("or_x= [%f]", cam->markers.at(0).pose.pose.orientation.x);
		//ROS_INFO("or_y= [%f]", cam->markers.at(0).pose.pose.orientation.y);
		//ROS_INFO("or_z= [%f]", cam->markers.at(0).pose.pose.orientation.z);
		//ROS_INFO("or_w= [%f]", cam->markers.at(0).pose.pose.orientation.w);

	 	bool is_marker_ok = true;

	  	tf::Quaternion qt;
	  	double roll , pitch , yaw;
	  	tf::quaternionMsgToTF(cam->markers.at(0).pose.pose.orientation, qt);
	  	tf::Matrix3x3(qt).getRPY(roll,pitch,yaw);

	  	if(std::isnan(roll) || std::isfinite(roll) || (cam->markers.at(0).pose.pose.position.z < 0))
	  		is_marker_ok = false;

	  	ex_old = ex;
	  	ey_old = ey;
	  	ez_old = ez;
	  	e_yaw_old = e_yaw;
	  	e_pitch_old = e_pitch;

	  	ex = x-xd;
		ey = y-yd;
		ez = z-zd;

		if (yaw > 0)
		{
			e_yaw = (fabs(yaw) -yaw_d);
		}
		else
			e_yaw = -(fabs(yaw) -yaw_d);

		e_pitch = pitch - pitch_d;


	  	e_yaw_int = e_yaw_int +e_yaw;

		if(z-zd >= 1.5 && ez<0)
			ez=0;

		markerDetected = true;
		//ROS_INFO("yaw: %f ",yaw);
		//ROS_INFO("Error: ex: %f ey: %f ez: %f eyaw: %f",ex,ey,ez,e_yaw);
    }
    else
    {
	  	//ROS_INFO("\n\n\n No marker is detected");
	  	ex = 0;
	  	ey = 0;
	  	ez = 0;
	  	e_yaw = 0;
	  	markerDetected = false;
    }
}

// Function to toggle the camera
void toggleCamera(bool & toggle,std_srvs::Empty *srv, ros::ServiceClient *client)
{
	if (toggle)
	{
		client->call(*srv);
    	toggle = false;
	}
}

// Function to force a dynamic random pose to the ardrone
void randomPos( geometry_msgs::Twist *cmd_vel_msg )
{
	cmd_vel_msg->linear.x = rand() % 1 - 0.5;
	cmd_vel_msg->linear.y = rand() % 1 - 0.5;
	cmd_vel_msg->linear.z = rand() % 1 - 0.5;
	cmd_vel_msg->angular.z = rand() % 1 - 0.5;

	return;
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "project_main_node");
	ros::NodeHandle n;
	ros::Rate loop_rate(10);
	// Dynamic Reconfiguration declaration
	dynamic_reconfigure::Server<project_main::dynparamsConfig> server;
	dynamic_reconfigure::Server<project_main::dynparamsConfig>::CallbackType f;
	f = boost::bind(&callback, _1, _2); //when the rqt reconfiguration has modified something the function "callback" is called
	server.setCallback(f);
	// Plublishers to control the Takeoff, Landing and Velocities of the ardrone
	ros::Publisher takeOff_pub = n.advertise<std_msgs::Empty>("/ardrone/takeoff",1);
	ros::Publisher land_pub = n.advertise<std_msgs::Empty>("/ardrone/land",1);
	ros::Publisher cmd_vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel",1);
	// general subscription to the ar_pose
	ros::Subscriber sub = n.subscribe("/ar_pose_marker", 1, Camera_Callback); //subscription to the topic "ar_pose_marker"
	//service -> to toggle the camera (/ardrone/togglecam)
	ros::ServiceClient client = n.serviceClient<std_srvs::Empty>("/ardrone/togglecam");
	std_srvs::Empty srv;

	// initial values of global variables
 	ex = 0;
	ey = 0;
	ez = 0;
	e_yaw = 0;
	// Initial Proportional Gains
	kp_x = 0;
	kp_y = 0;
	kp_z = 0;
	kp_yaw = 0;
	// Initial Derivative Gains
	kd_x = 0;
	kd_y = 0;
	kd_z = 0;
	kd_yaw = 0;
	// Initial integral Gains
	ki_x = 0;
	ki_y = 0;
	ki_z = 0;
	ki_yaw = 0;

    toggle = false;

    // Set of the desired positions for the estimation of the error and control
	xd = 0;
	yd = 0;
	zd = 1.0;
	yaw_d = 2*1.57;
	pitch_d = 0;

	bool subscriptionDone = false;

	int counter = 0;

	while (ros::ok())
	{
		//ROS_INFO("---------------\n\n");
		//setting up the Empty msgs for the dynamic reconfiguration of the Landing and Takeoff
		std_msgs::Empty emptyMsg;

		if(takeOff)
		{
			takeOff_pub.publish(emptyMsg);
		}
		if(landing)
		{
			land_pub.publish(emptyMsg);
			counter=0;
			if(counterToggle == 1)
				counterToggle = 0;
		}
		//setting up the Twist message
		geometry_msgs::Twist cmd_vel_msg;

		cmd_vel_msg.linear.x = x_vel;
		cmd_vel_msg.linear.y = y_vel;
		cmd_vel_msg.linear.z = z_vel;
		cmd_vel_msg.angular.x = pitch;
		cmd_vel_msg.angular.y = roll;
		cmd_vel_msg.angular.z = yaw;

		toggleCamera(toggle, &srv, &client);

		if(!frontController)
		{
			if (activateController)
			{	
				// PID Controller
				pid_x = ((float)ey*(float)kp_x) + (((float)ey-(float)ey_old)*(float)kd_x) + (((float)ey+(float)ey_old)*(float)ki_x);
				pid_y = ((float)ex*(float)kp_y) + (((float)ex-(float)ex_old)*(float)kd_y) + (((float)ex+(float)ex_old)*(float)ki_y);
				pid_z = ((float)ez*(float)kp_z) + (((float)ez-(float)ez_old)*(float)kd_z) + (((float)ez+(float)ez_old)*(float)ki_z);
				//pid_yaw = ((float)e_yaw*(float)kp_yaw) + (((float)e_yaw-(float)e_yaw_old)*(float)kd_yaw) + (((float)e_yaw+(float)e_yaw_old)*(float)ki_yaw);
				pid_yaw = ((float)e_yaw*(float)kp_yaw) + (((float)e_yaw-(float)e_yaw_old)*(float)kd_yaw);// + (((float)e_yaw_int)*(float)ki_yaw);


				takeOff = true;

				if(counterToggle == 0)
				{
					toggle = true; //first toggle to enable the bottom camera
					counterToggle = 1;
				}
				if (!errorOk())
				{
					sec_ini =ros::Time::now().toSec();
					sec_end = sec_ini;
				}
				else
				{
					sec_end =ros::Time::now().toSec();
					ROS_INFO("time elapsed for bottom controller: %f",(sec_end-sec_ini));
				}
				if( (sec_end-sec_ini) >= 5.0)
				{
					sec_ini = sec_end;
					toggle = true;
					frontController = true;
					counter = 0;
				}
				// Controller for the bottom camera
				
				cmd_vel_msg.linear.x = -pid_x;//(float)ey*(float)kp_x;
				cmd_vel_msg.linear.y = -pid_y;//(float)ex*(float)kp_y;
				cmd_vel_msg.linear.z = -pid_z;//(float)ez*(float)kp_z;
				cmd_vel_msg.angular.z = -pid_yaw;//(float)e_yaw*(float)kp_yaw;
 				ROS_INFO("Controlling. x vel: %f y vel: %f z vel: %f yaw vel: %f \n Error: ex: %f ey: %f ez: %f eyaw: %f",cmd_vel_msg.linear.x,cmd_vel_msg.linear.y,cmd_vel_msg.linear.z,cmd_vel_msg.angular.z,ex,ey,ez,e_yaw);
			}
			else if(controller)
			{
				// PID Controller

				pid_x = ((float)ey*(float)kp_x) + (((float)ey-(float)ey_old)*(float)kd_x) + (((float)ey+(float)ey_old)*(float)ki_x);
				pid_y = ((float)ex*(float)kp_y) + (((float)ex-(float)ex_old)*(float)kd_y) + (((float)ex+(float)ex_old)*(float)ki_y);
				pid_z = ((float)ez*(float)kp_z) + (((float)ez-(float)ez_old)*(float)kd_z) + (((float)ez+(float)ez_old)*(float)ki_z);
				//pid_yaw = ((float)e_yaw*(float)kp_yaw) + (((float)e_yaw-(float)e_yaw_old)*(float)kd_yaw) + (((float)e_yaw+(float)e_yaw_old)*(float)ki_yaw);
				pid_yaw = ((float)e_yaw*(float)kp_yaw) + (((float)e_yaw-(float)e_yaw_old)*(float)kd_yaw);// + (((float)e_yaw_int)*(float)ki_yaw);


				cmd_vel_msg.linear.x = -pid_x;//(float)ey*(float)kp_x;
				cmd_vel_msg.linear.y = -pid_y;//(float)ex*(float)kp_y;
				cmd_vel_msg.linear.z = -pid_z;//(float)ez*(float)kp_z;
				cmd_vel_msg.angular.z = -pid_yaw;//(float)e_yaw*(float)kp_yaw;

				counter=0;

				ROS_INFO("Controlling. x vel: %f y vel: %f z vel: %f yaw vel: %f \n Error: ex: %f ey: %f ez: %f eyaw: %f",cmd_vel_msg.linear.x,cmd_vel_msg.linear.y,cmd_vel_msg.linear.z,cmd_vel_msg.angular.z,ex,ey,ez,e_yaw);
			}
		}
		else //front controller activated
		{						
			
			if (!errorOk2())
			{
				sec_ini =ros::Time::now().toSec();
				sec_end = sec_ini;
			}
			else
			{
				sec_end =ros::Time::now().toSec();
				ROS_INFO("time elapsed for front controller: %f",(sec_end-sec_ini));
			}
			if( (sec_end-sec_ini) >= 5.0)
			{
				landing = true;
				takeOff = false;
				activateController = false;
			}

			// PID Controller
			pid_x = ((float)ez*(float)kp_x) + (((float)ez-(float)ez_old)*(float)kd_x) + (((float)ez+(float)ez_old)*(float)ki_x);
			pid_y = ((float)ex*(float)kp_y) + (((float)ex-(float)ex_old)*(float)kd_y) + (((float)ex+(float)ex_old)*(float)ki_y);
			pid_z = ((float)ey*(float)kp_z) + (((float)ey-(float)ey_old)*(float)kd_z) + (((float)ey+(float)ey_old)*(float)ki_z);
			//pid_yaw = ((float)e_yaw*(float)kp_yaw) + (((float)e_yaw-(float)e_yaw_old)*(float)kd_yaw) + (((float)e_yaw+(float)e_yaw_old)*(float)ki_yaw);
			pid_yaw = ((float)e_pitch*(float)kp_yaw) + (((float)e_pitch-(float)e_pitch_old)*(float)kd_yaw);// + (((float)e_yaw_int)*(float)ki_yaw);


			if(activateController)
			{
				cmd_vel_msg.linear.x = 0.5;
				cmd_vel_msg.linear.y = -pid_y;//(float)ex*(float)kp_y;
				cmd_vel_msg.linear.z = -pid_z;//(float)ey*(float)kp_z;
				if(z <= 1.5)
				{
					cmd_vel_msg.angular.z = -pid_yaw;//(float)e_yaw*(float)kp_yaw;
					cmd_vel_msg.linear.x = pid_x;//(float)ez*(float)kp_x;
				}
			}
			ROS_INFO("Controlling. x vel: %f y vel: %f z vel: %f yaw vel: %f \n Error: ex: %f ey: %f ez: %f e_pitch: %f",cmd_vel_msg.linear.x,cmd_vel_msg.linear.y,cmd_vel_msg.linear.z,cmd_vel_msg.angular.z,ex,ey,ez,e_pitch);

		}

		if(randomVel)
		 	randomPos(&cmd_vel_msg);

		if (cmd_vel_msg.linear.x > lin_limit)
			cmd_vel_msg.linear.x = lin_limit;
		if (cmd_vel_msg.linear.y > lin_limit)
			cmd_vel_msg.linear.y = lin_limit;
		if (cmd_vel_msg.linear.z > lin_limit)
			cmd_vel_msg.linear.z = lin_limit;
		if (cmd_vel_msg.angular.z > ang_limit)
			cmd_vel_msg.angular.z = ang_limit;
		
		cmd_vel_pub.publish(cmd_vel_msg);

		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
