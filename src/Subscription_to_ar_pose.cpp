#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <project_main/dynparamsConfig.h>
#include "ar_pose/ARMarkers.h"
#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>


//#include "ar_pose/ARMarkers.h"

// global variables declaration
double x_vel, y_vel, z_vel, pitch, roll, yaw;
bool takeOff , landing;
double x, y, z, xd, yd, zd;

void callback(project_main::dynparamsConfig &config, uint32_t level) {

	x_vel = config.x_vel; 
	y_vel = config.y_vel; 
	z_vel = config.z_vel; 
	pitch = config.pitch; 
	roll = config.roll; 
	yaw = config.yaw; 

	takeOff = config.takeOff;
	landing = config.landing;
	
	//ROS_INFO("%f %f %f %f %f %f %d %d \n",x_vel,y_vel,z_vel,pitch,roll,yaw,takeOff,landing);

}


void error_pose()
{
	double ex,ey,ez,e_tot; // declaring errors
	ex=xd-x;
	ey=yd-y;
	ez=zd-z;
	e_tot=sqrt(pow(ex,2)+pow(ey,2)+pow(ez,2));  //2-norm of the error

	ROS_INFO("Error: %f \n",e_tot);
  	ROS_INFO("Error along x axis: %f \n Error along y axis: %f \n Error along z axis: %f",ex,ey,ez);
  	ROS_INFO("[Desired position] \n x: %f \n y: %f \n z: %f \n",xd,yd,zd);

}


void AR_pose_marker_Callback(const ar_pose::ARMarkers::ConstPtr& cam )
{
  //checking if the vector is not empty, if it is we cannot work on it
	ROS_INFO("yeeeee\n\n\n");

  if (!cam->markers.empty())
  {
  	x=cam->markers.at(0).pose.pose.position.x;
  	y=cam->markers.at(0).pose.pose.position.y;
  	z=cam->markers.at(0).pose.pose.position.z;
  
  	 //I left this just to remember that we could extract also the pose ( expressed with quaternions)
	ROS_INFO("or_x= [%f]", cam->markers.at(0).pose.pose.orientation.x);
	ROS_INFO("or_y= [%f]", cam->markers.at(0).pose.pose.orientation.y);
	ROS_INFO("or_z= [%f]", cam->markers.at(0).pose.pose.orientation.z);
	ROS_INFO("or_w= [%f]", cam->markers.at(0).pose.pose.orientation.w);
	

 	error_pose(); //calling the function that computes the error
  }
  else
  	ROS_INFO("No marker is detected");

}


int main(int argc, char **argv)
{
	ros::init(argc, argv, "project_main_node");
	ros::NodeHandle n;
	ros::Rate loop_rate(100);

	dynamic_reconfigure::Server<project_main::dynparamsConfig> server;
	dynamic_reconfigure::Server<project_main::dynparamsConfig>::CallbackType f;
	f = boost::bind(&callback, _1, _2); //when the rqt reconfiguration has modified something the function "callback" is called
	server.setCallback(f);

	//ros::Subscriber sub = n.subscribe("ar_pose_marker", 1, AR_pose_marker_Callback); //subscription to the topic "ar_pose_marker"
	
	ros::Publisher takeOff_pub = n.advertise<std_msgs::Empty>("/ardrone/takeoff",1);
	ros::Publisher land_pub = n.advertise<std_msgs::Empty>("/ardrone/land",1);
	ros::Publisher cmd_vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel",1);

	ros::Subscriber sub = n.subscribe("/ar_pose_marker", 1, AR_pose_marker_Callback); //subscription to the topic "ar_pose_marker"

	xd = 0;
	yd = 0;
	zd = 1.0;

	while (ros::ok())
	{
		std_msgs::Empty emptyMsg;
		if(takeOff && !landing)
		{
			takeOff_pub.publish(emptyMsg);
		}
		if(landing && !takeOff)
		{
			land_pub.publish(emptyMsg);
		}
		//setting up the Twist message
		geometry_msgs::Twist cmd_vel_msg;
		cmd_vel_msg.linear.x = x_vel;
		cmd_vel_msg.linear.y = y_vel;
		cmd_vel_msg.linear.z = z_vel;
		cmd_vel_msg.angular.x = pitch;
		cmd_vel_msg.angular.y = roll;
		cmd_vel_msg.angular.z = yaw;
		cmd_vel_pub.publish(cmd_vel_msg);

		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}
